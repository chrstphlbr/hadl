//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.04.16 at 02:10:14 PM CEST 
//


package at.ac.tuwien.dsg.hadl.schema.core;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tCollabLink complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tCollabLink">
 *   &lt;complexContent>
 *     &lt;extension base="{http://at.ac.tuwien.dsg/hADL/hADLcore}tHADLarchElement">
 *       &lt;sequence>
 *         &lt;element name="objActionEndpoint" type="{http://www.w3.org/2001/XMLSchema}IDREF"/>
 *         &lt;element name="collabActionEndpoint" type="{http://www.w3.org/2001/XMLSchema}IDREF"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tCollabLink", propOrder = {
    "objActionEndpoint",
    "collabActionEndpoint"
})
public class TCollabLink
    extends THADLarchElement
{

    @XmlElement(required = true, type = Object.class)
    @XmlIDREF
    @XmlSchemaType(name = "IDREF")
    protected TAction objActionEndpoint;
    @XmlElement(required = true, type = Object.class)
    @XmlIDREF
    @XmlSchemaType(name = "IDREF")
    protected TAction collabActionEndpoint;

    /**
     * Gets the value of the objActionEndpoint property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public TAction getObjActionEndpoint() {
        return objActionEndpoint;
    }

    /**
     * Sets the value of the objActionEndpoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setObjActionEndpoint(TAction value) {
        this.objActionEndpoint = value;
    }

    /**
     * Gets the value of the collabActionEndpoint property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public TAction getCollabActionEndpoint() {
        return collabActionEndpoint;
    }

    /**
     * Sets the value of the collabActionEndpoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setCollabActionEndpoint(TAction value) {
        this.collabActionEndpoint = value;
    }

}
