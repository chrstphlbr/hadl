# README #

### What is this repository for? ###

* Quick summary - executing hADL (human Architecture Description Language) models
* Version 0.0.1

### How do I get set up? ###

* Summary of set up:
    * Subproject hADL.schema contains hADL XSDs and JAXB classes - run mvn clean install to obtain and register the jar you need for the hADL/runtimefw (below)
    * Subproject hADL.runtimefw provides a first framework for instantiating hADL models (see example model in hADL.schema project/resources/examples)

* Configuration: 
 None of the actual framework so far

* Dependencies:
     * hADL.runtimefw also requires JAXB 2.2.11 jars [https://github.com/gf-metro/jaxb]  (as the alternative dependency via maven doesn't work, see commented out section in the POM)

* How to run tests: 
Currently only a single JUnit test class that also acts as exemplary hADL runtimefw client


### Contribution guidelines ###

* Runnable but not very thoroughly tested code yet. Feel free to (write) test(s)
* Integration/Dependency Management: feel free to suggest changes to the MAVEN setup to:
    * figure out why the JAXB dependencies via maven don't work (creating a JAXBContext results in a NoSuchMethodException com.sun.xml.bind.v2.model.nav.ReflectionNavigator.getInstance() )

### Who do I talk to? ###

* Repo owner Christoph Dorn