package ac.at.tuwien.dsg.hadl.framework.test.runtime.gdrive;

import java.io.File;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import at.ac.tuwien.dsg.hadl.framework.runtime.impl.ModelTypesUtil;
import at.ac.tuwien.dsg.hadl.schema.core.HADLmodel;
import at.ac.tuwien.dsg.hadl.schema.core.ObjectFactory;
import at.ac.tuwien.dsg.hadl.schema.core.TActivityScope;
import at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement;
import at.ac.tuwien.dsg.hadl.schema.core.TStructure;
import at.ac.tuwien.dsg.hadl.schema.extension.google.TGoogleFileDescriptor;
import at.ac.tuwien.dsg.hadl.schema.extension.google.TGoogleUserDescriptor;
import at.ac.tuwien.dsg.hadl.schema.runtime.TResourceDescriptor;
import at.ac.tuwien.dsg.hadl.schema.runtime.TRuntimeStructure;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class GoogleModelLoader 
{
	private static String MODEL_FILE = "./src/test/resources/input/GoogleDrive.xml";
	private static String OUTPUT_PATH_PREFIX = "./src/test/resources/output/gdrive/";
	
	private HADLmodel model = null;
	private ModelTypesUtil mtu;
	
	public GoogleModelLoader()
	{
		try {
			JAXBContext jc = JAXBContext.newInstance( ObjectFactory.class,
					at.ac.tuwien.dsg.hadl.schema.executable.ObjectFactory.class); 
					//, at.ac.tuwien.dsg.adaf.hADLmodel.schema.runtime.ObjectFactory.class);

			Unmarshaller unmarshaller = jc.createUnmarshaller();			
			File xml = new File(MODEL_FILE);
			model = (HADLmodel) unmarshaller.unmarshal(xml);			
		} catch (JAXBException e) {			
			e.printStackTrace();
			model = null;
			fail();		
		}		
	}
	
	
	public HADLmodel getModel() {
		return model;
	}
	
	public void setModelTypesUtil(ModelTypesUtil mtu)
	{
		this.mtu = mtu;
	}

	public TActivityScope getDefaultFullScope()
	{
		return retrieveScopeById(model, "DefaultFullScope");
	}

	public TActivityScope getMinimalScope()
	{
		return retrieveScopeById(model, "MinimalScope");
	}
	
	public static TActivityScope retrieveScopeById(HADLmodel model, String scopeId)
	{
		for (TStructure struct : model.getHADLstructure())
		{
			for (TActivityScope actS : struct.getActivityScope())
			{
				if (actS.getId().equals(scopeId))
					return actS;
			}
		}
		return null;
	}
	
	public static void storeRuntimeModel(TRuntimeStructure struct, String outputTo)
	{
		try {
			at.ac.tuwien.dsg.hadl.schema.runtime.ObjectFactory of = new at.ac.tuwien.dsg.hadl.schema.runtime.ObjectFactory();
			JAXBContext jc = JAXBContext.newInstance("at.ac.tuwien.dsg.hadl.schema.core:at.ac.tuwien.dsg.hadl.schema.runtime:at.ac.tuwien.dsg.hadl.schema.extension.google");
			Marshaller m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE );
			m.marshal(of.createRuntimeStructure(struct), new File(OUTPUT_PATH_PREFIX+outputTo));
		} catch (JAXBException e) {			
			e.printStackTrace();
		}
	}
	
	public static void storeSpecModel(HADLmodel model, String outputTo)
	{
		try {			
			JAXBContext jc = JAXBContext.newInstance("at.ac.tuwien.dsg.hadl.schema.core:at.ac.tuwien.dsg.hadl.schema.executable");
			Marshaller m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE );
			m.marshal(model, new File(OUTPUT_PATH_PREFIX+outputTo));
		} catch (JAXBException e) {			
			e.printStackTrace();
		}
	}
	
	public static TGoogleFileDescriptor createFileDescriptor(String id)
	{
		TGoogleFileDescriptor gfd2 = new TGoogleFileDescriptor();
		gfd2.setName("Mock file"+id);
		gfd2.setId(id);
		return gfd2;
	}
	
	public static TGoogleUserDescriptor createUserDescriptor(String id)
	{
		TGoogleUserDescriptor gud1 = new TGoogleUserDescriptor();
		gud1.setEmail(id+"@google.com");
		gud1.setId(id);	
		return gud1;
	}
		
	public void addFileToMapping(List<Entry<THADLarchElement, TResourceDescriptor>> mapping, String fileId)
	{
		THADLarchElement fileType = mtu.getById("GoogleFile");
		assertNotNull(fileType);									
		mapping.add(new AbstractMap.SimpleEntry<THADLarchElement, TResourceDescriptor>(fileType, createFileDescriptor(fileId)));
	}
	
	public void addUserToMapping(List<Entry<THADLarchElement, TResourceDescriptor>> mapping, String userId)
	{
		THADLarchElement userType = mtu.getById("GoogleDriveUser");
		assertNotNull(userType);												
		mapping.add(new AbstractMap.SimpleEntry<THADLarchElement, TResourceDescriptor>(userType, createUserDescriptor(userId)));		
	}
}
