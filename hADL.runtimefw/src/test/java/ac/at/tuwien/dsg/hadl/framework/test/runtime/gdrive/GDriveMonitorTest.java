package ac.at.tuwien.dsg.hadl.framework.test.runtime.gdrive;

import static org.junit.Assert.*;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import rx.Observable;
import rx.functions.Action1;
import ac.at.tuwien.dsg.hadl.framework.test.runtime.LifecycleTester;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.LoadEvent;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.SensingScope;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.SensorFactory;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.HADLruntimeModel;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.ModelTypesUtil;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.SurrogateEvent;
import at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement;
import at.ac.tuwien.dsg.hadl.schema.core.TObjectRef;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalComponent;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObject;
import at.ac.tuwien.dsg.hadl.schema.runtime.TResourceDescriptor;

public class GDriveMonitorTest extends LifecycleTester {
	
	private GoogleModelLoader gml = new GoogleModelLoader();
	private TestingGoogleUserMockSensorFactory factory = new TestingGoogleUserMockSensorFactory();
	private final SensingScope sScope = new SensingScope();
	
	@Before
	public void setUp() throws Exception {
		
		super.setUp(gml.getModel(), gml.getMinimalScope(), factory);
		gml.setModelTypesUtil(mtu);		
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	
	@Test
	public void testLoadViaUser() throws InterruptedException
	{	
		factory.setSensorConfig(0);
		super.init();
		List<Entry<THADLarchElement, TResourceDescriptor>> mapping = new ArrayList<Map.Entry<THADLarchElement,TResourceDescriptor>>();
		gml.addUserToMapping(mapping, "user1");
		gml.addUserToMapping(mapping, "user2");
		increaseWaitDurationByMillis(500000); //debugging time
		increaseWaitDurationByMillis(5000);
		final CountDownLatch lock3 = new CountDownLatch(2);
		super.acquire(mapping, loadOnAcquire(lock3), null, null, 5000l);
		lock3.await(500000, TimeUnit.MILLISECONDS);
		GoogleModelLoader.storeRuntimeModel(hrm, "1_postLoad.xml");
		assertEquals(2, hrm.getLink().size());
		
		final CountDownLatch lock = new CountDownLatch(1);
		String prefix = "Unwire All ";
		increaseWaitDurationByMillis(5000);
		Observable<SurrogateEvent> obsUnwire = app.unwireAllActions();
		obsUnwire.subscribe(getDefaultOnNextSurrogateEventAction(prefix), getDefaultOnErrorAction(lock, prefix), getDefaultCompletedEventAction(lock, prefix));
		lock.await(5000, TimeUnit.MILLISECONDS);		
		GoogleModelLoader.storeRuntimeModel(hrm, "2_postUnwire.xml");
				
		final CountDownLatch lock2 = new CountDownLatch(1);
		prefix = "LoadViaFiles ";
		increaseWaitDurationByMillis(5000);
		Observable<LoadEvent> obsMon = monitor.loadFromAllInScopeOfType(mtu.getById("GoogleFile"), new SensingScope());
		obsMon.subscribe(getDefaultOnNextLoadEventAction(prefix), getDefaultOnErrorAction(lock2, prefix), getDefaultCompletedEventAction(lock2, prefix));
		lock2.await(5000, TimeUnit.MILLISECONDS);
		GoogleModelLoader.storeRuntimeModel(hrm, "3_postReload.xml");
		
		super.testLock.countDown();
	}
	
	@Test
	public void testLoadViaFile() throws InterruptedException
	{
		factory.setSensorConfig(1);
		super.init();
		List<Entry<THADLarchElement, TResourceDescriptor>> mapping = new ArrayList<Map.Entry<THADLarchElement,TResourceDescriptor>>();
		gml.addFileToMapping(mapping, "file1");
		
		increaseWaitDurationByMillis(5000);
		final CountDownLatch lock3 = new CountDownLatch(2);
		super.acquire(mapping, loadOnAcquire(lock3), null, null, 5000l);
		lock3.await(5000, TimeUnit.MILLISECONDS);
		GoogleModelLoader.storeRuntimeModel(hrm, "1_postLoad.xml");
		assertEquals(2, hrm.getObject().size());
		
		final CountDownLatch lock2 = new CountDownLatch(1);
		String prefix = "LoadViaUsers ";
		increaseWaitDurationByMillis(5000);
		Observable<LoadEvent> obsMon = monitor.loadFromAllInScopeOfType(mtu.getById("GoogleDriveUser"), new SensingScope());		
		obsMon.subscribe(getDefaultOnNextLoadEventAction(prefix), getDefaultOnErrorAction(lock2, prefix), getDefaultCompletedEventAction(lock2, prefix));
		lock2.await(5000, TimeUnit.MILLISECONDS);
		GoogleModelLoader.storeRuntimeModel(hrm, "3_postReload.xml");
	}
	
	@Test
	public void testLoadRemoveLoadLess() throws InterruptedException
	{
		factory.setSensorConfig(2);
		sScope.getObjectRefs().add((TObjectRef)mtu.getById("templateFile"));
		sScope.getObjectRefs().add((TObjectRef)mtu.getById("fileInfolder"));
		
		super.init();
		List<Entry<THADLarchElement, TResourceDescriptor>> mapping = new ArrayList<Map.Entry<THADLarchElement,TResourceDescriptor>>();
		gml.addFileToMapping(mapping, "file1");
		
		increaseWaitDurationByMillis(5000);
		final CountDownLatch lock3 = new CountDownLatch(2);
		super.acquire(mapping, loadOnAcquire(lock3), null, null, 5000l);
		lock3.await(5000, TimeUnit.MILLISECONDS);
		GoogleModelLoader.storeRuntimeModel(hrm, "1_postLoad.xml");
		assertEquals(2, hrm.getObject().size());				
		
		final CountDownLatch lock = new CountDownLatch(1);
		String prefix = "Remove TemplateFile Relation ";
		increaseWaitDurationByMillis(5000);
		List<Entry<TOperationalObject, TOperationalObject>> relations = new ArrayList<>();
		relations.add(new AbstractMap.SimpleEntry<TOperationalObject, TOperationalObject>(hrm.getObject().get(0), hrm.getObject().get(1)));
		Observable<SurrogateEvent> obsUnwire = app.buildRelations(relations, (TObjectRef)mtu.getById("templateFile"), true);
		obsUnwire.subscribe(getDefaultOnNextSurrogateEventAction(prefix), getDefaultOnErrorAction(lock, prefix), getDefaultCompletedEventAction(lock, prefix));
		lock.await(5000, TimeUnit.MILLISECONDS);		
		GoogleModelLoader.storeRuntimeModel(hrm, "2_postUnRelate.xml");
				
		factory.setSensorConfig(3);
		final CountDownLatch lock2 = new CountDownLatch(1);
		prefix = "LoadAgainViaFile ";
		increaseWaitDurationByMillis(5000);
		Observable<LoadEvent> obsScope = monitor.loadFrom(hrm.getObject().get(0), sScope);
		obsScope.subscribe(getDefaultOnNextLoadEventAction(prefix), getDefaultOnErrorAction(lock2, prefix), getDefaultCompletedEventAction(lock2, prefix));
		lock2.await(5000, TimeUnit.MILLISECONDS);
		GoogleModelLoader.storeRuntimeModel(hrm, "3_postReload.xml");
		
		super.testLock.countDown();
	}
	

	private Action1<SurrogateEvent> loadOnAcquire(final CountDownLatch lock)
	{
		return new Action1<SurrogateEvent>() {
			@Override
			public void call(SurrogateEvent t) {
				String prefix = "Loading ";							
				if (t.getOperationalElement() instanceof TOperationalObject)
				{					
					Observable<LoadEvent> obsScope = monitor.loadFrom((TOperationalObject)t.getOperationalElement(), sScope);
					obsScope.subscribe(getDefaultOnNextLoadEventAction(prefix), getDefaultOnErrorAction(lock, prefix), getDefaultCompletedEventAction(lock, prefix));
				}
				else if (t.getOperationalElement() instanceof TOperationalComponent)
				{
					Observable<LoadEvent> obsScope = monitor.loadFrom((TOperationalComponent)t.getOperationalElement(), sScope);
					obsScope.subscribe(getDefaultOnNextLoadEventAction(prefix), getDefaultOnErrorAction(lock, prefix), getDefaultCompletedEventAction(lock, prefix));
				}
			}
		};
	}
	
	
}
