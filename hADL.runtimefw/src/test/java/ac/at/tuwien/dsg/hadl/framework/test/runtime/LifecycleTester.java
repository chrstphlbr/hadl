package ac.at.tuwien.dsg.hadl.framework.test.runtime;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import rx.Observable;
import rx.Observer;
import rx.functions.Action0;
import rx.functions.Action1;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.LoadEvent;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.SensorFactory;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.Executable2OperationalTransformer;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.HADLLinkageConnector;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.HADLruntimeModel;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.HADLruntimeMonitor;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.ModelTypesUtil;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.RuntimeRegistry;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.SurrogateFactoryResolver;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.FactoryResolvingException;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.StatusEvent;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.SurrogateEvent;
import at.ac.tuwien.dsg.hadl.schema.core.HADLmodel;
import at.ac.tuwien.dsg.hadl.schema.core.TActivityScope;
import at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement;
import at.ac.tuwien.dsg.hadl.schema.runtime.TResourceDescriptor;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provides;

public class LifecycleTester 
{
	protected Injector injector;
	protected HADLLinkageConnector app;
	private TActivityScope scope;
	protected ExecutorService dispatcher;
	protected ModelTypesUtil mtu;
	protected HADLmodel model;
	protected HADLruntimeModel hrm;
	protected HADLruntimeMonitor monitor;
	protected CountDownLatch testLock;
	private long waitTestCompletion = 5000; 
	protected static final Logger logger = LogManager.getLogger(LifecycleTester.class);
	
	
	public void setUp(final HADLmodel model, TActivityScope scope, final SensorFactory sf) throws Exception
	{
		if (dispatcher == null || dispatcher.isShutdown())
			dispatcher = Executors.newSingleThreadExecutor();
		assertNotNull(model);
		this.model = model;
		assertNotNull(scope);
		this.scope = scope;
		
		testLock = new CountDownLatch(1);
		
		final Injector baseInjector = Guice.createInjector(new AbstractModule() {
			@Override
			protected void configure() {				
			}
			
			@Provides
			@Singleton
			Executable2OperationalTransformer getTransformer()
			{				
				return new Executable2OperationalTransformer();
			}
			
			@Provides 
			@Singleton
			ModelTypesUtil getTypesUtil()
			{
				mtu = new ModelTypesUtil();
				mtu.init(model);
				return mtu;
			}					
		});
		
		injector = Guice.createInjector(new AbstractModule() {
			@Override
			protected void configure() {
				bind(HADLLinkageConnector.class).in(Singleton.class);
				bind(HADLruntimeMonitor.class).in(Singleton.class);
			}			
			
			@Provides
			@Singleton
			Executable2OperationalTransformer getTransformer()
			{				
				return baseInjector.getInstance(Executable2OperationalTransformer.class);
			}
			
			@Provides
			@Singleton
			RuntimeRegistry getRegistry()
			{
				return new RuntimeRegistry();				
			}
			
			@Provides 
			@Singleton
			ModelTypesUtil getTypesUtil()
			{
				return baseInjector.getInstance(ModelTypesUtil.class);
			}
			
			@Provides
			@Singleton
			SurrogateFactoryResolver getFactoryResolver()
			{
				SurrogateFactoryResolver sfr = new SurrogateFactoryResolver();
				try {
					sfr.resolveFactoriesFrom(model);
				} catch (FactoryResolvingException e) {					
					e.printStackTrace();
				}
				return sfr; 				
			}
			
			@Provides
			@Singleton
			HADLruntimeModel getRuntimeModel()
			{
				hrm = new HADLruntimeModel();
				baseInjector.injectMembers(hrm);
				return hrm; 							
			}
			
			@Provides
			@Singleton
			SensorFactory getSensorFactory()
			{				
				baseInjector.injectMembers(sf);
				return sf;
			}
			
		});			
		app = injector.getInstance(HADLLinkageConnector.class);
		monitor = injector.getInstance(HADLruntimeMonitor.class);
	}
		
	public void tearDown() throws Exception {
		
		app.shutdown();
		try {
			assertTrue("Test Run Timeout", testLock.await(waitTestCompletion, TimeUnit.MILLISECONDS));
		} catch (InterruptedException e1) {			
			e1.printStackTrace();
		}		
		injector = null;
		scope = null;		
		app = null;
		monitor=null;				
		dispatcher.shutdownNow();
		dispatcher = null;	
	}

	protected void increaseWaitDurationByMillis(long millis)
	{
		waitTestCompletion+=millis;
	}
	
	protected void init()
	{
		monitor.init(scope);
		Observable<StatusEvent> obsScope = app.init(scope);		
		obsScope.subscribe(new Observer<StatusEvent>() {

			@Override
			public void onCompleted() {	
				logger.info("HADLLinkageConnector: completed");	
				testLock.countDown();
			}

			@Override
			public void onError(Throwable e) {
				logger.error("HADLLinkageConnector: encountered error",e);							
				fail();
				testLock.countDown();
			}

			@Override
			public void onNext(StatusEvent t) {				
				logger.debug("HADLLinkageConnector: "+t.getStatus());
				assertFalse("Status INCOMPLETED", t.getStatus().toString().endsWith("INCOMPLETED"));
			}			
		});	
	}
	
	protected void acquire(List<Entry<THADLarchElement, TResourceDescriptor>> mapping, Action1<SurrogateEvent> onNext, Action1<Throwable> onError, Action0 onComplete, long timeout)
	{
		increaseWaitDurationByMillis(timeout);
		final CountDownLatch lock = new CountDownLatch(1);
		String prefix = "Aquiring ";
		Observable<SurrogateEvent> obs = app.acquireResourcesAsElements(mapping);
		
		obs.subscribe(onNext!= null ? onNext : getDefaultOnNextSurrogateEventAction(prefix),
					onError != null ? onError : getDefaultOnErrorAction(lock, prefix),
					onComplete != null ? onComplete : getDefaultCompletedEventAction(lock, prefix)		
				);
		
		try {
			assertTrue("Timeout", lock.await(timeout, TimeUnit.MILLISECONDS));
		} catch (InterruptedException e1) {
			logger.error("Acquire Timeout Interrupted",e1);
		}
	}
	
	
	protected Action1<LoadEvent> getDefaultOnNextLoadEventAction(final String logPrefix)
	{
		return new Action1<LoadEvent>() {
			@Override
			public void call(LoadEvent t) {				
				logger.trace((logPrefix != null ? logPrefix : "")+"from: "+t.getOriginId()+" of: "+Arrays.toString(t.getDescriptors().toArray()));
			}
		};
	}
	
	protected Action1<SurrogateEvent> getDefaultOnNextSurrogateEventAction(final String logPrefix)
	{
		return new Action1<SurrogateEvent>() {
			@Override
			public void call(SurrogateEvent t) {				
				logger.trace((logPrefix != null ? logPrefix : "")+t.getStatus() +" : "+t.getSource().getHref());
			}
		};
	}
	
	protected Action0 getDefaultCompletedEventAction(final CountDownLatch lock, final String logPrefix)
	{
		return new Action0() {
			@Override
			public void call() {				
				
				logger.trace((logPrefix != null ? logPrefix : "")+"Observer Completed");
				if (lock != null)
					lock.countDown();
			}
		};
	}
	
	protected Action1<Throwable> getDefaultOnErrorAction(final CountDownLatch lock, final String logPrefix)
	{
		return new Action1<Throwable>() {
			@Override
			public void call(Throwable t1) {
				logger.error((logPrefix != null ? logPrefix : "")+"Observer encountered error: ", t1);
				fail((logPrefix != null ? logPrefix : "")+"Observer obtained Exception");
				if (lock != null)
					lock.countDown();
			}
		};
	}
	
	
}
