package ac.at.tuwien.dsg.hadl.framework.test.runtime.gdrive;

import javax.inject.Inject;

import at.ac.tuwien.dsg.hadl.framework.runtime.events.ICollabObjectSensor;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.ICollabSensor;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.ICollaboratorSensor;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.SensorFactory;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.ModelTypesUtil;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.InsufficientModelInformationException;
import at.ac.tuwien.dsg.hadl.schema.core.TCollabObject;
import at.ac.tuwien.dsg.hadl.schema.core.TCollaborator;
import at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement;

public class TestingGoogleUserMockSensorFactory implements SensorFactory {

	@Inject private ModelTypesUtil mtu;
	
	private int config = 0;
	
	public void setSensorConfig(int config)
	{
		this.config = config;
	}
	
	@Override
	public ICollabSensor getInstance(THADLarchElement type)
			throws InsufficientModelInformationException {		
		return new SimpleTestingSensor(mtu, config);
	}

	@Override
	public ICollaboratorSensor getInstance(TCollaborator type)
			throws InsufficientModelInformationException {
		return new SimpleTestingSensor(mtu, config);
	}

	@Override
	public ICollabObjectSensor getInstance(TCollabObject type)
			throws InsufficientModelInformationException {
		return new SimpleTestingSensor(mtu, config);
	}

}
