package ac.at.tuwien.dsg.hadl.framework.test.runtime.surrogates;

import rx.Observable;
import rx.subjects.BehaviorSubject;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.AbstractAsyncObjectSurrogate;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.SimpleSyncObjectSurrogate;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.ESurrogateStatus;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.SurrogateEvent;
import at.ac.tuwien.dsg.hadl.schema.core.TAction;
import at.ac.tuwien.dsg.hadl.schema.core.TActivityScope;
import at.ac.tuwien.dsg.hadl.schema.core.TObjectRef;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalCollabLink;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalComponent;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalConnector;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObject;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObjectRef;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalState;

public class DefaultAcceptingObjectSurrogate extends AbstractAsyncObjectSurrogate  
{
	private void sleepAndFire(BehaviorSubject<SurrogateEvent> subject, SurrogateEvent se)
	{
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {			
			e.printStackTrace();
		}
		subject.onNext(se);	
		subject.onCompleted();
	}

	@Override
	public void asyncBegin(BehaviorSubject<SurrogateEvent> subject) {
		if (oc.getResourceDescriptor().isEmpty() || oc.getResourceDescriptor().get(0).getId() == null)
			this.oc.setState(TOperationalState.DESCRIBED_EXISTING);
		sleepAndFire(subject, new SurrogateEvent(thisRef, ESurrogateStatus.STARTING_SUCCESS, this.oc));
	}

	@Override
	public void asyncStop(BehaviorSubject<SurrogateEvent> subject) {
		sleepAndFire(subject, new SurrogateEvent(thisRef, ESurrogateStatus.STOPPING_SUCCESS, this.oc));
	}

	@Override
	public void asyncRelease(BehaviorSubject<SurrogateEvent> subject) {
		this.oc.setState(TOperationalState.DESCRIBED_NONEXISTING);
		SurrogateEvent se = new SurrogateEvent(thisRef, ESurrogateStatus.RELEASING_SUCCESS, this.oc);
		this.oc = null;
		sleepAndFire(subject, se);
	}

	@Override
	public void asyncAcquire(TActivityScope forScope,
			TOperationalObject surrogateFor,
			BehaviorSubject<SurrogateEvent> subject) {	
		if (!oc.getResourceDescriptor().isEmpty() && oc.getResourceDescriptor().get(0).getId() != null)
			this.oc.setState(TOperationalState.DESCRIBED_EXISTING);
		sleepAndFire(subject, new SurrogateEvent(thisRef, ESurrogateStatus.ACQUIRING_SUCCESS, this.oc));
	}

	@Override
	public void asyncLinkTo(TAction localAction,
			TOperationalConnector oppositeElement, TAction oppositeAction,
			BehaviorSubject<SurrogateEvent> subject, TOperationalCollabLink link) {
		link.setState(TOperationalState.DESCRIBED_EXISTING);
		sleepAndFire(subject, new SurrogateEvent(thisRef, ESurrogateStatus.REWIRING_SUCCESS, link));
	}

	@Override
	public void asyncLinkTo(TAction localAction,
			TOperationalComponent oppositeElement, TAction oppositeAction,
			BehaviorSubject<SurrogateEvent> subject, TOperationalCollabLink link) {
		link.setState(TOperationalState.DESCRIBED_EXISTING);
		sleepAndFire(subject, new SurrogateEvent(thisRef, ESurrogateStatus.REWIRING_SUCCESS, link));
	}

	@Override
	public void asyncDisconnectFrom(TAction localAction,
			TOperationalComponent oppositeElement,
			BehaviorSubject<SurrogateEvent> subject, TOperationalCollabLink link) {
		link.setState(TOperationalState.DESCRIBED_NONEXISTING);
		sleepAndFire(subject, new SurrogateEvent(thisRef, ESurrogateStatus.REWIRING_SUCCESS, link));
	}

	@Override
	public void asyncDisconnectFrom(TAction localAction,
			TOperationalConnector oppositeElement,
			BehaviorSubject<SurrogateEvent> subject, TOperationalCollabLink link) {
		link.setState(TOperationalState.DESCRIBED_NONEXISTING);
		sleepAndFire(subject, new SurrogateEvent(thisRef, ESurrogateStatus.REWIRING_SUCCESS, link));		
	}

	@Override
	public void asyncRelating(TOperationalObject oppositeElement,
			TObjectRef relationType, boolean isRelationOrigin,
			boolean doRemove, BehaviorSubject<SurrogateEvent> subject, TOperationalObjectRef ref) {
		if (ref!= null)
		{
			if (doRemove) 
				ref.setState(TOperationalState.DESCRIBED_NONEXISTING);
			else
				ref.setState(TOperationalState.DESCRIBED_EXISTING);
		}
		sleepAndFire(subject, new SurrogateEvent(thisRef, ESurrogateStatus.RELATING_SUCCESS, ref));
		
	}
	
	
}
