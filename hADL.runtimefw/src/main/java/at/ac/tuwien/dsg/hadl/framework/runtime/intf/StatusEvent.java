package at.ac.tuwien.dsg.hadl.framework.runtime.intf;

import java.util.ArrayList;
import java.util.List;

import at.ac.tuwien.dsg.hadl.schema.runtime.TResourceDescriptor;

public class StatusEvent 
{
	private String scopeId;
	private EhADLstatus status;
	private List<TResourceDescriptor> entities = new ArrayList<TResourceDescriptor>();
	
	
	public StatusEvent(String scopeId, EhADLstatus status) {
		super();
		this.scopeId = scopeId;
		this.status = status;
	}
	
	public StatusEvent(String scopeId, EhADLstatus status, List<TResourceDescriptor> entities) {
		super();
		this.scopeId = scopeId;
		this.status = status;
		this.entities = entities;
	}
	
	
	
	public List<TResourceDescriptor> getEntities() {
		return entities;
	}

	public void addEntities(TResourceDescriptor entity) {
		this.entities.add(entity);
	}

	public String getScopeId() {
		return scopeId;
	}
	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}
	public EhADLstatus getStatus() {
		return status;
	}
	public void setStatus(EhADLstatus status) {
		this.status = status;
	}
	
	
}
