package at.ac.tuwien.dsg.hadl.framework.runtime.events;

public class SensorEvent 
{
	private ESensorStatus status;
	private Exception optEx = null;
	private ICollabSensor source = null;
	
	
	public Exception getOptionalEx() {
		return optEx;
	}
	public void setOptionalEx(Exception optEx) {
		this.optEx = optEx;
	}		
	
	public ESensorStatus getStatus() {
		return status;
	}
	public void setStatus(ESensorStatus status) {
		this.status = status;
	}

	public ICollabSensor getSource() {
		return source;
	}
	public void setSource(ICollabSensor source) {
		this.source = source;
	}

	public SensorEvent(ESensorStatus status, Exception optEx,
			ICollabSensor source) {
		super();
		this.status = status;
		this.optEx = optEx;
		this.setSource(source);
	}
	public SensorEvent(ESensorStatus status, ICollabSensor source) {
		super();
		this.status = status;
		this.setSource(source);
	}

	public enum ESensorStatus {

		ACQUIRING_INPROGRESS,
		ACQUIRING_FAILED,
		ACQUIRING_SUCCESS,
		ON_STAND_BY,
		LOADING,
		LOADING_FAILED,
		LOADING_SUCCESS,
		CONTINUOUS_SENSING,
		DEFUNCT
	}
}
