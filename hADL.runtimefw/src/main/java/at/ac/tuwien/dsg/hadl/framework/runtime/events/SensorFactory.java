package at.ac.tuwien.dsg.hadl.framework.runtime.events;

import at.ac.tuwien.dsg.hadl.framework.runtime.intf.InsufficientModelInformationException;
import at.ac.tuwien.dsg.hadl.schema.core.TCollabObject;
import at.ac.tuwien.dsg.hadl.schema.core.TCollaborator;
import at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalComponent;

public interface SensorFactory {

	public ICollabSensor getInstance(THADLarchElement type) throws InsufficientModelInformationException;	
	
	public ICollaboratorSensor getInstance(TCollaborator type) throws InsufficientModelInformationException;
	
	public ICollabObjectSensor getInstance(TCollabObject type) throws InsufficientModelInformationException;
		
}
