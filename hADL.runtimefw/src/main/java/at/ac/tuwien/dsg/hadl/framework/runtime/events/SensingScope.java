package at.ac.tuwien.dsg.hadl.framework.runtime.events;

import java.util.HashSet;

import at.ac.tuwien.dsg.hadl.schema.core.TAction;
import at.ac.tuwien.dsg.hadl.schema.core.TCollabLink;
import at.ac.tuwien.dsg.hadl.schema.core.TCollabRef;
import at.ac.tuwien.dsg.hadl.schema.core.TObjectRef;

public class SensingScope 
{
	private HashSet<TAction> actions = new HashSet<>();
	private HashSet<TCollabLink> links = new HashSet<>();
	private HashSet<TCollabRef> collabRefs = new HashSet<>();
	private HashSet<TObjectRef> objectRefs = new HashSet<>();
	private HashSet<TCollabLink> substructureWires = new HashSet<TCollabLink>();
	
	public HashSet<TAction> getActions() {
		return actions;
	}
	public void setActions(HashSet<TAction> actions) {
		this.actions = actions;
	}
	public HashSet<TCollabLink> getLinks() {
		return links;
	}
	public void setLinks(HashSet<TCollabLink> links) {
		this.links = links;
	}
	public HashSet<TCollabRef> getCollabRefs() {
		return collabRefs;
	}
	public void setCollabRefs(HashSet<TCollabRef> collabRefs) {
		this.collabRefs = collabRefs;
	}
	public HashSet<TObjectRef> getObjectRefs() {
		return objectRefs;
	}
	public void setObjectRefs(HashSet<TObjectRef> objectRefs) {
		this.objectRefs = objectRefs;
	}
	public HashSet<TCollabLink> getSubstructureWires() {
		return substructureWires;
	}
	public void setSubstructureWires(HashSet<TCollabLink> substructureWires) {
		this.substructureWires = substructureWires;
	}
	
	public void mergeWith(SensingScope mergeFrom)
	{
		getActions().addAll(mergeFrom.getActions());
		getLinks().addAll(mergeFrom.getLinks());
		getCollabRefs().addAll(mergeFrom.getCollabRefs());
		getObjectRefs().addAll(mergeFrom.getObjectRefs());
		getSubstructureWires().addAll(mergeFrom.getSubstructureWires());
	}
	
	public boolean isCompletelyEmpty()
	{
		return actions.isEmpty() && links.isEmpty() && collabRefs.isEmpty() && objectRefs.isEmpty() && substructureWires.isEmpty();
	}
}
