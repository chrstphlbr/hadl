package at.ac.tuwien.dsg.hadl.framework.runtime.intf;

import at.ac.tuwien.dsg.hadl.schema.runtime.TSurrogateInstanceRef;
import rx.Observable;

public interface ISurrogate 
{	
	
	public Observable<SurrogateEvent> begin(); 
	
	public Observable<SurrogateEvent> stop();
	
	public Observable<SurrogateEvent> release();
	
	public void setSurrogateInstanceRef(TSurrogateInstanceRef ref);
	
}
