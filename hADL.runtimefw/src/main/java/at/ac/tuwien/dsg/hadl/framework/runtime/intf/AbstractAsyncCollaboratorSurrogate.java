package at.ac.tuwien.dsg.hadl.framework.runtime.intf;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import rx.Observable;
import rx.subjects.BehaviorSubject;
import at.ac.tuwien.dsg.hadl.schema.core.TAction;
import at.ac.tuwien.dsg.hadl.schema.core.TActivityScope;
import at.ac.tuwien.dsg.hadl.schema.core.TCollabRef;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalCollabLink;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalCollabRef;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalComponent;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalConnector;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObject;
import at.ac.tuwien.dsg.hadl.schema.runtime.TSurrogateInstanceRef;

public abstract class AbstractAsyncCollaboratorSurrogate implements ICollaboratorSurrogate 
{	
		private ExecutorService dispatcher = Executors.newSingleThreadExecutor();			
		protected TOperationalComponent oc1 = null;
		protected TOperationalConnector oc2 = null;
		protected TActivityScope scope = null;
		protected TSurrogateInstanceRef thisRef = null;
		
	@Override
	public void setSurrogateInstanceRef(TSurrogateInstanceRef ref) {
		thisRef = ref;
	}	
		
	@Override
	public Observable<SurrogateEvent> begin() {
		final BehaviorSubject<SurrogateEvent> statusSubject = BehaviorSubject.create();
		dispatcher.execute(new Runnable(){
			@Override
			public void run() { asyncBegin(statusSubject);
			}			
		});						
		return statusSubject;
	}
	
	public abstract void asyncBegin(BehaviorSubject<SurrogateEvent> subject);

	@Override
	public Observable<SurrogateEvent> stop() {
		final BehaviorSubject<SurrogateEvent> statusSubject = BehaviorSubject.create();
		dispatcher.execute(new Runnable(){
			@Override
			public void run() { asyncStop(statusSubject);
			}			
		});						
		return statusSubject;
	}
	
	public abstract void asyncStop(BehaviorSubject<SurrogateEvent> subject);

	@Override
	public Observable<SurrogateEvent> release() {		
		final BehaviorSubject<SurrogateEvent> statusSubject = BehaviorSubject.create();
		dispatcher.execute(new Runnable(){
			@Override
			public void run() { asyncRelease(statusSubject);
			}			
		});						
		return statusSubject;
	}
	
	public abstract void asyncRelease(BehaviorSubject<SurrogateEvent> subject);

	@Override
	public Observable<SurrogateEvent> acquire(final TActivityScope forScope,
			final TOperationalComponent surrogateFor) {
		this.scope = forScope;
		this.oc1 = surrogateFor;
		final BehaviorSubject<SurrogateEvent> statusSubject = BehaviorSubject.create();
		dispatcher.execute(new Runnable(){
			@Override
			public void run() { asyncAcquire(forScope, surrogateFor, statusSubject);
			}			
		});						
		return statusSubject;
	}
	
	public abstract void asyncAcquire(TActivityScope forScope,
			TOperationalComponent surrogateFor, BehaviorSubject<SurrogateEvent> subject);

	@Override
	public Observable<SurrogateEvent> acquire(final TActivityScope forScope,
			final TOperationalConnector surrogateFor) {		
		this.scope = forScope;
		this.oc2 = surrogateFor;
		final BehaviorSubject<SurrogateEvent> statusSubject = BehaviorSubject.create();
		dispatcher.execute(new Runnable(){
			@Override
			public void run() { asyncAcquire(forScope, surrogateFor, statusSubject);
			}			
		});						
		return statusSubject;
	}
	
	public abstract void asyncAcquire(TActivityScope forScope,
			TOperationalConnector surrogateFor, BehaviorSubject<SurrogateEvent> subject);

	@Override
	public Observable<SurrogateEvent> linkTo(final TAction localAction,
			final TOperationalObject oppositeElement, final TAction oppositeAction, final TOperationalCollabLink link) {	
		final BehaviorSubject<SurrogateEvent> statusSubject = BehaviorSubject.create();
		dispatcher.execute(new Runnable(){
			@Override
			public void run() { asyncLinkTo(localAction, oppositeElement, oppositeAction, statusSubject, link);
			}			
		});						
		return statusSubject;
	}
	
	public abstract void asyncLinkTo(TAction localAction,
			TOperationalObject oppositeElement, TAction oppositeAction, BehaviorSubject<SurrogateEvent> subject, TOperationalCollabLink link);
	
	@Override
	public Observable<SurrogateEvent> disconnectFrom(final TAction localAction,
			final TOperationalObject oppositeElement, final TOperationalCollabLink link) {
		final BehaviorSubject<SurrogateEvent> statusSubject = BehaviorSubject.create();
		dispatcher.execute(new Runnable(){
			@Override
			public void run() { asyncDisconnectFrom( localAction, oppositeElement, statusSubject, link);
			}			
		});						
		return statusSubject;
	}
	
	public abstract void asyncDisconnectFrom(TAction localAction,
			TOperationalObject oppositeElement, BehaviorSubject<SurrogateEvent> subject, TOperationalCollabLink link);


	@Override
	public Observable<SurrogateEvent> relating(
			final TOperationalConnector oppositeElement, final TCollabRef relationType,
			final boolean isRelationOrigin, final boolean doRemove, final TOperationalCollabRef ref) {
		final BehaviorSubject<SurrogateEvent> statusSubject = BehaviorSubject.create();
		dispatcher.execute(new Runnable(){
			@Override
			public void run() { asyncRelating(oppositeElement, relationType, isRelationOrigin, doRemove, statusSubject, ref);
			}			
		});						
		return statusSubject;
	}
	
	public abstract void asyncRelating(TOperationalConnector oppositeElement, TCollabRef relationType,
			boolean isRelationOrigin, boolean doRemove, BehaviorSubject<SurrogateEvent> subject, TOperationalCollabRef ref);

	@Override
	public Observable<SurrogateEvent> relating(
			final TOperationalComponent oppositeElement, final TCollabRef relationType,
			final boolean isRelationOrigin, final boolean doRemove, final TOperationalCollabRef ref) {
		final BehaviorSubject<SurrogateEvent> statusSubject = BehaviorSubject.create();
		dispatcher.execute(new Runnable(){
			@Override
			public void run() { asyncRelating(oppositeElement, relationType, isRelationOrigin, doRemove, statusSubject, ref);
			}			
		});						
		return statusSubject;
	}
	
	public abstract void asyncRelating(TOperationalComponent oppositeElement, TCollabRef relationType,
			boolean isRelationOrigin, boolean doRemove, BehaviorSubject<SurrogateEvent> subject, TOperationalCollabRef ref);



	
}
