package at.ac.tuwien.dsg.hadl.framework.runtime.events;

import at.ac.tuwien.dsg.hadl.schema.core.TActivityScope;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalComponent;
import at.ac.tuwien.dsg.hadl.schema.runtime.TResourceDescriptor;
import rx.Observable;

public interface ICollabSensor {

	public Observable<LoadEvent> loadOnce(SensingScope scope);
	
}