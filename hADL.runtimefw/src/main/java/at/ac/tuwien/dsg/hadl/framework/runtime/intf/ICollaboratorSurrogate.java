package at.ac.tuwien.dsg.hadl.framework.runtime.intf;

import at.ac.tuwien.dsg.hadl.schema.core.*;
import at.ac.tuwien.dsg.hadl.schema.runtime.*;
import rx.Observable;

public interface ICollaboratorSurrogate extends ISurrogate {

	public Observable<SurrogateEvent> acquire(TActivityScope forScope, TOperationalComponent surrogateFor);
	
	public Observable<SurrogateEvent> acquire(TActivityScope forScope, TOperationalConnector surrogateFor);
	
	public Observable<SurrogateEvent> linkTo(TAction localAction, TOperationalObject oppositeElement, TAction oppositeAction, TOperationalCollabLink link);
	
	public Observable<SurrogateEvent> disconnectFrom(TAction localAction, TOperationalObject oppositeElement, TOperationalCollabLink link);

	public Observable<SurrogateEvent> relating(TOperationalConnector oppositeElement, TCollabRef relationType, boolean isRelationOrigin, boolean doRemove, TOperationalCollabRef ref);
	
	public Observable<SurrogateEvent> relating(TOperationalComponent oppositeElement, TCollabRef relationType, boolean isRelationOrigin, boolean doRemove, TOperationalCollabRef ref);
		
}
