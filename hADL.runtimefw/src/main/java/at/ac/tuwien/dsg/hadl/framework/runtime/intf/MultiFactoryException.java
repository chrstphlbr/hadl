package at.ac.tuwien.dsg.hadl.framework.runtime.intf;

import java.util.Hashtable;
import java.util.Map.Entry;

public class MultiFactoryException extends Exception
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private MultiException<FactoryResolvingException> exceptions = new MultiException<FactoryResolvingException>();

	public Hashtable<String, FactoryResolvingException> getExceptions() {
		return exceptions.getExceptions();
	}

	public MultiFactoryException() {
		super();		
	}
	
	public MultiFactoryException(String correlationId, FactoryResolvingException ex) {
		super();		
		addException(correlationId, ex);
	}

	public boolean isEmpty() {
		return exceptions.isEmpty();
	}

	public void mergeIntoThis(MultiFactoryException me) {
		exceptions.mergeIntoThis(me.exceptions);
	}
	
	public void addException(String correlationId,
			FactoryResolvingException exception) {
		exceptions.addException(correlationId, exception);
	}

	@Override
	public String getMessage() {
		StringBuffer msg = new StringBuffer();
		for (Entry<String,FactoryResolvingException> fre : exceptions.getExceptions().entrySet())
		{
			msg.append(fre.getKey()+" : "+fre.getValue().getMessage()+"\r\n");			
		}
		return msg.toString();
	}
	
	
}
