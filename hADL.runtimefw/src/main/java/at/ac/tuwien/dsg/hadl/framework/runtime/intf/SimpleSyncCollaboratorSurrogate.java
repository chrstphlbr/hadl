package at.ac.tuwien.dsg.hadl.framework.runtime.intf;

import rx.Observable;
import at.ac.tuwien.dsg.hadl.schema.core.TAction;
import at.ac.tuwien.dsg.hadl.schema.core.TActivityScope;
import at.ac.tuwien.dsg.hadl.schema.core.TCollabRef;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalCollabLink;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalCollabRef;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalComponent;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalConnector;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObject;
import at.ac.tuwien.dsg.hadl.schema.runtime.TSurrogateInstanceRef;

public class SimpleSyncCollaboratorSurrogate implements ICollaboratorSurrogate{

	protected TOperationalComponent oc1 = null;
	protected TOperationalConnector oc2 = null;
	protected TActivityScope scope = null;
	protected TSurrogateInstanceRef thisRef = null;

	public SimpleSyncCollaboratorSurrogate() {
		super();
	}

	@Override
	public Observable<SurrogateEvent> begin() {
	
		return Observable.just(new SurrogateEvent(thisRef, ESurrogateStatus.STARTING_SUCCESS, oc1 == null ? oc2 : oc1));
	}

	@Override
	public Observable<SurrogateEvent> stop() {
		return Observable.just(new SurrogateEvent(thisRef, ESurrogateStatus.STOPPING_SUCCESS, oc1 == null ? oc2 : oc1));
	}

	@Override
	public Observable<SurrogateEvent> release() {
		
		SurrogateEvent se = new SurrogateEvent(thisRef, ESurrogateStatus.RELEASING_SUCCESS, oc1 == null ? oc2 : oc1);		
		oc1 = null;
		oc2 = null;
		return Observable.just(se);
	}

	@Override
	public Observable<SurrogateEvent> acquire(TActivityScope forScope, TOperationalComponent surrogateFor) {
		this.scope = forScope;
		this.oc1 = surrogateFor;
		return Observable.just(new SurrogateEvent(thisRef, ESurrogateStatus.ACQUIRING_SUCCESS, oc1));
	}

	@Override
	public Observable<SurrogateEvent> acquire(TActivityScope forScope, TOperationalConnector surrogateFor) {
		this.scope = forScope;
		this.oc2 = surrogateFor;
		return Observable.just(new SurrogateEvent(thisRef, ESurrogateStatus.ACQUIRING_SUCCESS, oc2));
	}

	@Override
	public Observable<SurrogateEvent> linkTo(TAction localAction, TOperationalObject oppositeElement, TAction oppositeAction, TOperationalCollabLink link) {
		return Observable.just(new SurrogateEvent(thisRef, ESurrogateStatus.REWIRING_SUCCESS, link));
	}

	@Override
	public Observable<SurrogateEvent> disconnectFrom(TAction localAction, TOperationalObject oppositeElement, TOperationalCollabLink link) {
		return Observable.just(new SurrogateEvent(thisRef, ESurrogateStatus.REWIRING_SUCCESS, link));
	}

	@Override
	public Observable<SurrogateEvent> relating(TOperationalConnector oppositeElement, TCollabRef relationType, boolean isRelationOrigin, boolean doRemove, TOperationalCollabRef ref) {
		return Observable.just(new SurrogateEvent(thisRef, ESurrogateStatus.RELATING_SUCCESS, ref));
	}

	@Override
	public Observable<SurrogateEvent> relating(TOperationalComponent oppositeElement, TCollabRef relationType, boolean isRelationOrigin, boolean doRemove, TOperationalCollabRef ref) {
		return Observable.just(new SurrogateEvent(thisRef, ESurrogateStatus.RELATING_SUCCESS, ref));
	}

	@Override
	public String toString() {		
		String s1 = (oc1 == null && oc2 == null) ? "none" : (oc1 == null ? "["+ oc2.getName() + " Id: "+ oc2.getId()+ "]" : "["+ oc1.getName() + " Id: "+ oc1.getId()+ "]");  
		String s2 = scope == null ? "none" : "["+ scope.getName() + " Id: "+ scope.getId()+ "]";
		return "AbstractCollaboratorSurrogate [oc=" + s1 + ", scope="
				+ s2 + "]";
	}

	@Override
	public void setSurrogateInstanceRef(TSurrogateInstanceRef ref) {
		this.thisRef = ref;
	}



}