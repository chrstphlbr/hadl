package at.ac.tuwien.dsg.hadl.framework.runtime.intf;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import rx.Observable;
import rx.subjects.BehaviorSubject;
import at.ac.tuwien.dsg.hadl.schema.core.TAction;
import at.ac.tuwien.dsg.hadl.schema.core.TActivityScope;
import at.ac.tuwien.dsg.hadl.schema.core.TObjectRef;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalCollabLink;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalComponent;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalConnector;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObject;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObjectRef;
import at.ac.tuwien.dsg.hadl.schema.runtime.TSurrogateInstanceRef;

public abstract class AbstractAsyncObjectSurrogate implements IObjectSurrogate {
	
	private ExecutorService dispatcher = Executors.newSingleThreadExecutor();	
	protected TSurrogateInstanceRef thisRef = null;
	protected TOperationalObject oc = null;
	protected TActivityScope scope = null;
	
	@Override
	public void setSurrogateInstanceRef(TSurrogateInstanceRef ref) {
		thisRef = ref;
	}
	
	@Override
	public Observable<SurrogateEvent> begin() {
		final BehaviorSubject<SurrogateEvent> statusSubject = BehaviorSubject.create();
		dispatcher.execute(new Runnable(){
			@Override
			public void run() { asyncBegin(statusSubject);
			}			
		});						
		return statusSubject;
	}
	
	public abstract void asyncBegin(BehaviorSubject<SurrogateEvent> subject);

	@Override
	public Observable<SurrogateEvent> stop() {
		final BehaviorSubject<SurrogateEvent> statusSubject = BehaviorSubject.create();
		dispatcher.execute(new Runnable(){
			@Override
			public void run() { asyncStop(statusSubject);
			}			
		});						
		return statusSubject;
	}
	
	public abstract void asyncStop(BehaviorSubject<SurrogateEvent> subject);

	@Override
	public Observable<SurrogateEvent> release() {
		final BehaviorSubject<SurrogateEvent> statusSubject = BehaviorSubject.create();
		dispatcher.execute(new Runnable(){
			@Override
			public void run() { asyncRelease(statusSubject);
			}			
		});						
		return statusSubject;
	}
	
	public abstract void asyncRelease(BehaviorSubject<SurrogateEvent> subject);

	@Override
	public Observable<SurrogateEvent> acquire(final TActivityScope forScope,
			final TOperationalObject surrogateFor) {		
		this.scope = forScope;
		this.oc = surrogateFor;
		final BehaviorSubject<SurrogateEvent> statusSubject = BehaviorSubject.create();
		dispatcher.execute(new Runnable(){
			@Override
			public void run() { asyncAcquire(forScope, surrogateFor, statusSubject);
			}			
		});						
		return statusSubject;
	}

	public abstract void asyncAcquire(TActivityScope forScope,
			TOperationalObject surrogateFor, BehaviorSubject<SurrogateEvent> subject);

	@Override
	public Observable<SurrogateEvent> linkTo(final TAction localAction,
			final TOperationalConnector oppositeElement, final TAction oppositeAction, final TOperationalCollabLink link) {
		final BehaviorSubject<SurrogateEvent> statusSubject = BehaviorSubject.create();
		dispatcher.execute(new Runnable(){
			@Override
			public void run() { asyncLinkTo(localAction, oppositeElement, oppositeAction, statusSubject, link);
			}			
		});						
		return statusSubject;
	}
	
	public abstract void asyncLinkTo(TAction localAction,
			TOperationalConnector oppositeElement, TAction oppositeAction, BehaviorSubject<SurrogateEvent> subject, TOperationalCollabLink link);

	@Override
	public Observable<SurrogateEvent> linkTo(final TAction localAction,
			final TOperationalComponent oppositeElement, final TAction oppositeAction, final TOperationalCollabLink link) {
		final BehaviorSubject<SurrogateEvent> statusSubject = BehaviorSubject.create();
		dispatcher.execute(new Runnable(){
			@Override
			public void run() { asyncLinkTo(localAction, oppositeElement, oppositeAction, statusSubject, link);
			}			
		});						
		return statusSubject;
	}
	
	public abstract void asyncLinkTo(TAction localAction,
			TOperationalComponent oppositeElement, TAction oppositeAction, BehaviorSubject<SurrogateEvent> subject, TOperationalCollabLink link);


	@Override
	public Observable<SurrogateEvent> disconnectFrom(final TAction localAction,
			final TOperationalComponent oppositeElement, final TOperationalCollabLink link) {
		final BehaviorSubject<SurrogateEvent> statusSubject = BehaviorSubject.create();
		dispatcher.execute(new Runnable(){
			@Override
			public void run() { asyncDisconnectFrom( localAction, oppositeElement, statusSubject, link);
			}			
		});						
		return statusSubject;
	}
	
	public abstract void asyncDisconnectFrom(TAction localAction,
			TOperationalComponent oppositeElement, BehaviorSubject<SurrogateEvent> subject, TOperationalCollabLink link);


	@Override
	public Observable<SurrogateEvent> disconnectFrom(final TAction localAction,
			final TOperationalConnector oppositeElement, final TOperationalCollabLink link) {
		final BehaviorSubject<SurrogateEvent> statusSubject = BehaviorSubject.create();
		dispatcher.execute(new Runnable(){
			@Override
			public void run() { asyncDisconnectFrom( localAction, oppositeElement, statusSubject, link);
			}			
		});						
		return statusSubject;
	}
	
	public abstract void asyncDisconnectFrom(TAction localAction,
			TOperationalConnector oppositeElement, BehaviorSubject<SurrogateEvent> subject, TOperationalCollabLink link);


	@Override
	public Observable<SurrogateEvent> relating(
			final TOperationalObject oppositeElement, final TObjectRef relationType,
			final boolean isRelationOrigin, final boolean doRemove, final TOperationalObjectRef ref) {
		final BehaviorSubject<SurrogateEvent> statusSubject = BehaviorSubject.create();
		dispatcher.execute(new Runnable(){
			@Override
			public void run() { asyncRelating(  oppositeElement, relationType, doRemove, isRelationOrigin, statusSubject, ref);
			}			
		});						
		return statusSubject;
	}
	
	public abstract void asyncRelating(TOperationalObject oppositeElement, TObjectRef relationType,
			boolean isRelationOrigin, boolean doRemove, BehaviorSubject<SurrogateEvent> subject, TOperationalObjectRef ref);




	
	
}
