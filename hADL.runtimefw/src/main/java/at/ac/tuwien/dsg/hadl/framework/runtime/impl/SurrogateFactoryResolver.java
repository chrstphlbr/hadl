package at.ac.tuwien.dsg.hadl.framework.runtime.impl;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.xml.bind.JAXBElement;

import at.ac.tuwien.dsg.hadl.framework.runtime.intf.FactoryResolvingException;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.MultiFactoryException;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.SurrogateFactory;
import at.ac.tuwien.dsg.hadl.schema.core.HADLmodel;
import at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement;
import at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement.Extension;
import at.ac.tuwien.dsg.hadl.schema.core.TStructure;
import at.ac.tuwien.dsg.hadl.schema.executable.TExecutableRefExtension;
import at.ac.tuwien.dsg.hadl.schema.executable.TExecutables;
import at.ac.tuwien.dsg.hadl.schema.executable.TSurrogate;



public class SurrogateFactoryResolver 
{
	
	
	private Hashtable<String,SurrogateFactory> hADLType2factory = new Hashtable<String,SurrogateFactory>();
	private Hashtable<String,SurrogateFactory> surrSpecId2factory = new Hashtable<String,SurrogateFactory>();
	
	public List<SurrogateFactory> resolveFactoryFrom(TExecutableRefExtension extRef) throws MultiFactoryException
	{
		List<SurrogateFactory> surrList = new ArrayList<SurrogateFactory>();
		MultiFactoryException mfe = new MultiFactoryException();
		int count = extRef.getExecutableViaSurrogate().size();
		if (count == 0)
		{			
			mfe.addException(""+extRef.hashCode(), new FactoryResolvingException("No Executable via Surrogate information given, nothing to resolve"));
		}
		else
		{				
			for (Object obj : extRef.getExecutableViaSurrogate())
			{
				TSurrogate surr = null;
//				if (obj instanceof JAXBElement)
//				{
//					surr = ((JAXBElement<TSurrogate>)obj).getValue();
//				}
//				else 
				if (obj instanceof TSurrogate)
					surr = (TSurrogate)obj;
				else
					throw new RuntimeException("Unexpected Object Type: "+obj.getClass());
				try {
					surrList.add(resolveFactoryFrom(surr));
				} catch (FactoryResolvingException e) {
					mfe.addException(surr.getId(), e);
				}
			}
		}
		if (mfe.getExceptions().size() >= count) //only exceptions
		{
			// log this
			throw mfe;
		}
		else
		{
			// log errors, return resolvable factories			
			return surrList;
		}		
	}
	
	public SurrogateFactory resolveFactoryFrom(TSurrogate surrogateSpec) throws FactoryResolvingException
	{
		String clazzName = surrogateSpec.getSurrogateFactoryFQN();
		if (surrSpecId2factory.containsKey(surrogateSpec.getId()))
		{
			return surrSpecId2factory.get(surrogateSpec.getId());
		} // else not processed, but factory might exist already		
		
		if (clazzName == null)
			throw new FactoryResolvingException("Missing SurrogateFactory identifier - (e.g., FactoryClass FQN in Surrogate element)");
		if (hADLType2factory.containsKey(clazzName) )
		{
			surrSpecId2factory.put(surrogateSpec.getId(), hADLType2factory.get(clazzName));
			return hADLType2factory.get(clazzName);
		}
		else
		{
			try {
				for (Class<?> intf : Class.forName(clazzName).getInterfaces())
				{
					if (intf.getCanonicalName().equals(SurrogateFactory.class.getCanonicalName()))
					{
						SurrogateFactory factory = (SurrogateFactory)Class.forName(clazzName).newInstance();
						hADLType2factory.put(clazzName, factory);
						surrSpecId2factory.put(surrogateSpec.getId(), factory);
						return factory;
					}
				}
				throw new FactoryResolvingException("Provided FactoryClass FQN in hADLexecutable element doesn't implement SurrogateFactory interface");
			} catch (ClassNotFoundException e) {
				throw new FactoryResolvingException(e);				
			} catch (SecurityException e) {
				throw new FactoryResolvingException(e);
			} catch (InstantiationException e) {
				throw new FactoryResolvingException(e);
			} catch (IllegalAccessException e) {
				throw new FactoryResolvingException(e);
			} catch (IllegalArgumentException e) {
				throw new FactoryResolvingException(e);
			}
		}					 	
	} 
	
	public List<SurrogateFactory> resolveFactoryFrom(THADLarchElement archElement) throws MultiFactoryException
	{
		// search through extensions for TExecutableRefExtension 
		List<TExecutableRefExtension> refs = new ArrayList<TExecutableRefExtension>();
		for (Extension ext : archElement.getExtension())
		{
			Object obj = ext.getAny();

			if (obj instanceof JAXBElement)
				obj = ((JAXBElement)obj).getValue();
			if (obj instanceof TExecutableRefExtension)
			{
				refs.add((TExecutableRefExtension)obj);
			}

		}
		List<SurrogateFactory> surrList = new ArrayList<SurrogateFactory>();
		MultiFactoryException mfe = new MultiFactoryException();
		if (refs.size() == 0)
		{			
			mfe.addException(archElement.getId(), new FactoryResolvingException("No TExecutableRefExtension or subelement given, nothing to resolve"));
		}
		for (TExecutableRefExtension ref : refs)
		{
			try {
				surrList.addAll(resolveFactoryFrom(ref));
			} catch (MultiFactoryException e) {
				mfe.mergeIntoThis(e);
			}
		}
		if (surrList.isEmpty())
			throw mfe;
		else 
			return surrList;		
	}
	
	//  for eager factory setup, throws on first error
	public void resolveFactoriesFrom(HADLmodel model) throws FactoryResolvingException
	{
		resolveFactoriesFromElement(model);
		for (TStructure struct : model.getHADLstructure())
		{
			resolveFactoriesFromElement(struct);
		}
	}
	
	private void resolveFactoriesFromElement(THADLarchElement el) throws FactoryResolvingException 
	{
		for (Extension ext : el.getExtension())
		{
			Object obj = ext.getAny();

			if (obj instanceof JAXBElement)
				obj = ((JAXBElement)obj).getValue();
			if (obj instanceof TExecutables)
			{
				for (TSurrogate surr : ((TExecutables)obj).getSurrogate())
				{
					resolveFactoryFrom(surr);
				}
			}

		}
	}
}

